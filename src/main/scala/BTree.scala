
trait SimpleTreeSet {
  def depth: Int
  def + (x: Int): SimpleTreeSet
  def contains (x: Int): Boolean
  def foreach (f: Int => Unit): Unit
}

class Tree extends SimpleTreeSet {

  def depth: Int = this match {
    case Empty => 0
    case Node(_, left, right) =>
      1 + {
        if (left.depth > right.depth) left.depth else right.depth
      }
  }

  def contains(x: Int): Boolean = this match {
    case Empty => false
    case Node(value, l, r) =>
      if (x > value) r.contains(x)
      else if (x < value) l.contains(x)
      else true
  }

  def foreach (f: Int => Unit): Unit = this match {
    case Empty => ()
    case Node(v, l, r) =>
      f(v)
      l.foreach(f)
      r.foreach(f)

  }

  def + (x: Int): Tree = {

    def recursiveVisitor (root: Tree): Tree = {
      root match {
        case Node(v, lBranch, rBranch) if (x > v) => Node (v, lBranch, recursiveVisitor(rBranch))
        case Node(v, lBranch, rBranch) if (x < v) => Node (v, recursiveVisitor(lBranch), rBranch)
        case Node(v, _, _) if (x == v) => root
        case Empty => Node(x, Empty, Empty)
      }
    }

    recursiveVisitor(this)
  }

}

case object Empty extends Tree
case class Node(value: Int, left: Tree, right: Tree) extends Tree


object BTree extends App {


  var root: Tree =
    Node(8,
      Node(3,
        Node(1,
          Empty,
          Empty),
        Node(4,
          Node(5,
            Empty,
            Empty),
          Node(7,
            Empty,
            Empty))),
    Node(10,
      Node(9,
        Empty,
        Empty),
      Empty
    )
  )

  root.foreach((x)=> print(x + " "))
  println()

  val a1 = 0
  root = root + a1
  println (s"Add $a1, depth ${root.depth} contains $a1 is ${root.contains(a1)}")
  root.foreach((x)=> print(x + " "))
  println()

  val a2 = 20
  root = root + a2
  println (s"Add $a2, depth ${root.depth} contains $a2 is ${root.contains(a2)}")
  root.foreach((x)=> print(x + " "))
  println()

  val a3 = -10
  root = root + a3
  println (s"Add $a3, depth ${root.depth} contains $a3 is ${root.contains(a3)}")
  root.foreach((x)=> print(x + " "))
  println()

  val a4 = -10
  root = root + a4
  println (s"Add $a4, depth ${root.depth} contains $a4 is ${root.contains(a4)}")
  root.foreach((x)=> print(x + " "))
  println()

}